import React from "react";
import logo from "../logo.svg";
import NavBar from "./NavBar";
import Forms from "./Forms";
import {Card, CardImg, CardImgOverlay, CardTitle} from "reactstrap";
let tutorTitle  = {
    color: 'white'
}
let tutorials = [
    {id: 1, title: "Tutorial 1", image: 'assets/images/caspar-camille-rubin-fPkvU7RDmCo-unsplash.jpg', link: ""},
    {id: 2, title: "Tutorial 2", image: 'assets/images/neslihan-gunaydin-BduDcrySLKM-unsplash.jpg', link: ""},
    {id: 3, title: "Tutorial 3", image: 'assets/images/kelsey-knight-udj2tD3WKsY-unsplash.jpg', link: ""},
    {id: 4, title: "Tutorial 4", image: 'assets/images/rob-walsh-Xn7YLHs8QNQ-unsplash.jpg', link: ""},
    {id: 5, title: "Tutorial 5", image: 'assets/images/tim-collins-oJYUA1g72AQ-unsplash.jpg', link: ""},
    {id: 6, title: "Tutorial 6", image: 'assets/images/allie-newgpclgEZI-unsplash.jpg', link: ""},
    {id: 7, title: "Tutorial 7", image: 'assets/images/andrew-neel-cckf4TsHAuw-unsplash.jpg', link: ""},
    {id: 8, title: "Tutorial 8", image: 'assets/images/serjan-midili-1FKHY4ZOWD0-unsplash.jpg', link: ""},
];

export const NewFrontPage =
    <React.Fragment>
        <div className="container-fluid">
            <div className="row">
                <div className="col-12 text-center">
                    <h1>Welcome to my website on learning ReactStrap</h1>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h2>List of tutorials</h2>
                </div>
            </div>
            <div className="row">
                {tutorials.map((t) => {
                    return (
                        <div key={t.id} className="col-md-3">
                            <Card>
                                <CardImg width="100%" src={t.image} alt={t.title} />
                                <CardImgOverlay>
                                    <CardTitle className="card-title">
                                        <h4 style={tutorTitle}>{t.title}</h4></CardTitle>
                                </CardImgOverlay>
                            </Card>
                        </div>
                    )
                })}
            </div>
            <div className="row">
                <div className="col-12">
                    <p>Page 1, 2, 3</p>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h2>References and resources</h2>
                    <ol>
                       <li><a href="https://www.techiediaries.com/react-bootstrap/">
                           React Bootstrap Tutorial: Integrating Bootstrap 4 with React
                       </a></li>
                        <li><a href="https://unsplash.com/">Images courtesy</a></li>
                        <li><a href="https://reactstrap.github.io/">Reactstrap GitHub</a> </li>
                    </ol>
                </div>
            </div>
        </div>
    </React.Fragment>

export const DefaultHomePage =
    <React.Fragment>
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer">
                    Learn React
                </a>
            </header>
        </div>
    </React.Fragment>

export const NewHomePage =
    <React.Fragment>
        <div>
            <NavBar/>
            <br/>
            <div className="container">
                <div className="row">
                    <div className="col-lg-offset-2 col-lg-10">
                        <Forms/>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>

